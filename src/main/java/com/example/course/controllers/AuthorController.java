package com.example.course.controllers;

import com.example.course.respositories.AuthorRepo;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AuthorController {

    private AuthorRepo authorRepo;

    public AuthorController(AuthorRepo authorRepo){
        this.authorRepo = authorRepo;
    }

    @RequestMapping("/authors")
    public String getAuthors(Model model){
        model.addAttribute("authors", authorRepo.findAll());
        return "authors";
    }
}

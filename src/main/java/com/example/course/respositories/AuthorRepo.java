package com.example.course.respositories;

import com.example.course.domain.Author;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface AuthorRepo extends CrudRepository<Author, Long> {
}

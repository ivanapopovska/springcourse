package com.example.course.respositories;

import com.example.course.domain.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


public interface BookRepo extends CrudRepository<Book, Long> {
}

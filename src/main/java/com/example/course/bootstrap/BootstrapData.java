package com.example.course.bootstrap;

import com.example.course.domain.Author;
import com.example.course.domain.Book;
import com.example.course.domain.Publisher;
import com.example.course.respositories.AuthorRepo;
import com.example.course.respositories.BookRepo;
import com.example.course.respositories.PublisherRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootstrapData implements CommandLineRunner {

    private final AuthorRepo authorRepo;
    private final BookRepo bookRepo;
    private final PublisherRepo publisherRepo;

    public BootstrapData(AuthorRepo authorRepo, BookRepo bookRepo, PublisherRepo publisherRepo) {
        this.authorRepo = authorRepo;
        this.bookRepo = bookRepo;
        this.publisherRepo = publisherRepo;
    }

    @Override
    public void run(String... args) throws Exception {

        Author author = new Author("Eric", "Evans");
        Book book = new Book("DDD", "123123");
        author.getBooks().add(book);
        book.getAuthors().add(author);
        authorRepo.save(author);
        bookRepo.save(book);

        Publisher publisher = new Publisher("Ivana", "adreda","Macedonia", "Skopje", "1000");
        publisher.getBooks().add(book);
        publisherRepo.save(publisher);
        book.setPublisher(publisher);
        bookRepo.save(book);

        Author author1 = new Author("Rod", "Johnson");
        Book book1 = new Book("TTT", "456456");
        author1.getBooks().add(book1);
        book1.getAuthors().add(author1);
        book1.setPublisher(publisher);
        publisher.getBooks().add(book1);
        authorRepo.save(author1);
        bookRepo.save(book1);



        System.out.println("Started with bootstrap");
        System.out.println("Number of books"+ bookRepo.count());
        System.out.println("Number of books for publisher"+ publisher.getBooks().size());
    }


}
